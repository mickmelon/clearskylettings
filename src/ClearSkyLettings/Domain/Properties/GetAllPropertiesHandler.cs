﻿using ClearSkyLettings.Data;
using ClearSkyLettings.Domain.Properties.Queries;
using ClearSkyLettings.Models;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ClearSkyLettings.Domain.Properties
{
    public class GetAllPropertiesHandler : IRequestHandler<GetAllPropertiesQuery, IEnumerable<Property>>
    {
        private readonly ApplicationDbContext _context;

        public GetAllPropertiesHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Property>> Handle(GetAllPropertiesQuery request, CancellationToken cancellationToken)
        {
            return await _context.Properties
                .Include(p => p.PropertyType)
                .ToListAsync();
        }
    }
}
