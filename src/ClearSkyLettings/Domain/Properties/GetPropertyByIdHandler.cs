﻿using ClearSkyLettings.Data;
using ClearSkyLettings.Domain.Properties.Queries;
using ClearSkyLettings.Models;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace ClearSkyLettings.Domain.Properties
{
    public class GetPropertyByIdHandler : IRequestHandler<GetPropertyByIdQuery, Property>
    {
        private readonly ApplicationDbContext _context;

        public GetPropertyByIdHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Property> Handle(GetPropertyByIdQuery request, CancellationToken cancellationToken)
        {
            return await _context.Properties
                .Include(p => p.PropertyType)
                .FirstOrDefaultAsync(p => p.Id == request.PropertyId);
        }
    }
}
