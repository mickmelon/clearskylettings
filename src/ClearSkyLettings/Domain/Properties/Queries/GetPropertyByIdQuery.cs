﻿using ClearSkyLettings.Models;
using MediatR;

namespace ClearSkyLettings.Domain.Properties.Queries
{
    public class GetPropertyByIdQuery : IRequest<Property>
    {
        public long PropertyId { get; set; }

        public GetPropertyByIdQuery(long propertyId)
        {
            PropertyId = propertyId;
        }
    }
}
