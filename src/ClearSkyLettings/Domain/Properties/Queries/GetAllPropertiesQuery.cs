﻿using ClearSkyLettings.Models;
using MediatR;
using System.Collections.Generic;

namespace ClearSkyLettings.Domain.Properties.Queries
{
    public class GetAllPropertiesQuery : IRequest<IEnumerable<Property>>
    {
    }
}
