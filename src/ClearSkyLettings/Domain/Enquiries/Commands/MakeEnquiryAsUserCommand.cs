﻿using MediatR;

namespace ClearSkyLettings.Domain.Enquiries.Commands
{
    public class MakeEnquiryAsUserCommand : IRequest<bool>
    {
        public long PropertyId { get; set; }
        public string UserId { get; set; }

        public MakeEnquiryAsUserCommand(long propertyId, string userId)
        {
            PropertyId = propertyId;
            UserId = userId;
        }
    }
}
