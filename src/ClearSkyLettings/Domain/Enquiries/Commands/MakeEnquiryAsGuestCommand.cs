﻿using MediatR;

namespace ClearSkyLettings.Domain.Enquiries.Commands
{
    public class MakeEnquiryAsGuestCommand : IRequest<bool>
    {
        public long PropertyId { get; set; }
        public string Email { get; set; }

        public MakeEnquiryAsGuestCommand(long propertyId, string email)
        {
            PropertyId = propertyId;
            Email = email;
        }
    }
}
