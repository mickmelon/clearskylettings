﻿using ClearSkyLettings.Data;
using ClearSkyLettings.Domain.Enquiries.Queries;
using ClearSkyLettings.Models;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace ClearSkyLettings.Domain.Enquiries
{
    public class GetUserEnquiryByIdHandler : IRequestHandler<GetUserEnquiryByIdQuery, Enquiry>
    {
        private readonly ApplicationDbContext _context;

        public GetUserEnquiryByIdHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Enquiry> Handle(GetUserEnquiryByIdQuery request, CancellationToken cancellationToken)
        {
            return await _context.Enquiries
                .Include(e => e.Property)
                .Include(e => e.User)
                .FirstOrDefaultAsync(m => m.Id == request.EnquiryId && m.UserId == request.UserId);
        }
    }
}
