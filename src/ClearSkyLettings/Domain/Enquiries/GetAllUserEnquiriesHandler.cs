﻿using ClearSkyLettings.Data;
using ClearSkyLettings.Domain.Enquiries.Queries;
using ClearSkyLettings.Models;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ClearSkyLettings.Domain.Enquiries
{
    public class GetAllUserEnquiriesHandler : IRequestHandler<GetAllUserEnquiriesQuery, IEnumerable<Enquiry>>
    {
        private readonly ApplicationDbContext _context;

        public GetAllUserEnquiriesHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Enquiry>> Handle(GetAllUserEnquiriesQuery request, CancellationToken cancellationToken)
        {
            return await _context.Enquiries
                .Include(e => e.Property)
                .Where(e => e.UserId == request.UserId)
                .ToListAsync();
        }
    }
}
