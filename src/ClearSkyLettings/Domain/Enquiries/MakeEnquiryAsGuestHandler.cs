﻿using ClearSkyLettings.Domain.Enquiries.Commands;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace ClearSkyLettings.Domain.Enquiries
{
    public class MakeEnquiryAsGuestHandler : IRequestHandler<MakeEnquiryAsGuestCommand, bool>
    {
        public async Task<bool> Handle(MakeEnquiryAsGuestCommand request, CancellationToken cancellationToken)
        {
            // Send email to staff
            return true;
        }
    }
}
