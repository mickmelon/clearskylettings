﻿using ClearSkyLettings.Data;
using ClearSkyLettings.Domain.Enquiries.Commands;
using ClearSkyLettings.Models;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ClearSkyLettings.Domain.Enquiries
{
    public class MakeEnquiryAsUserHandler : IRequestHandler<MakeEnquiryAsUserCommand, bool>
    {
        private readonly ApplicationDbContext _context;

        public MakeEnquiryAsUserHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<bool> Handle(MakeEnquiryAsUserCommand request, CancellationToken cancellationToken)
        {
            var propertyExists = await _context.Properties.AnyAsync(p => p.Id == request.PropertyId);
            if (!propertyExists)
            {
                return false;
            }

            var alreadyEnquiredAboutProperty = await _context.Enquiries
                .AnyAsync(e => e.PropertyId == request.PropertyId && e.UserId == request.UserId);
            if (alreadyEnquiredAboutProperty)
            {
                return false;
            }

            var enquiry = new Enquiry
            {
                DateEnquired = DateTime.Now,
                PropertyId = request.PropertyId,
                UserId = request.UserId
            };

            await _context.AddAsync(enquiry);
            await _context.SaveChangesAsync();

            // Send email to staff

            return true;
        }
    }
}
