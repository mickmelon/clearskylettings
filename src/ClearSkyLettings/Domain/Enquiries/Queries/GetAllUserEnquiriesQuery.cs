﻿using ClearSkyLettings.Models;
using MediatR;
using System.Collections.Generic;

namespace ClearSkyLettings.Domain.Enquiries.Queries
{
    public class GetAllUserEnquiriesQuery : IRequest<IEnumerable<Enquiry>>
    {
        public string UserId { get; set; }

        public GetAllUserEnquiriesQuery(string userId)
        {
            UserId = userId;
        }
    }
}
