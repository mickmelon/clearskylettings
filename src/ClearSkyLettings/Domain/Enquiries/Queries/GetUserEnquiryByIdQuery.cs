﻿using ClearSkyLettings.Models;
using MediatR;

namespace ClearSkyLettings.Domain.Enquiries.Queries
{
    public class GetUserEnquiryByIdQuery : IRequest<Enquiry>
    {
        public string UserId { get; set; }
        public long EnquiryId { get; set; }

        public GetUserEnquiryByIdQuery(string userId, long enquiryId)
        {
            UserId = userId;
            EnquiryId = enquiryId;
        }
    }
}
