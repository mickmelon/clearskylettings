﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace ClearSkyLettings.Data.Migrations
{
    public partial class AddPropertyAndTypes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PropertyTypes",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PropertyTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Properties",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Address = table.Column<string>(nullable: true),
                    MonthlyPrice = table.Column<decimal>(nullable: false),
                    Deposit = table.Column<decimal>(nullable: false),
                    DateAdded = table.Column<DateTime>(nullable: false),
                    IsFurnished = table.Column<bool>(nullable: false),
                    NumberBedrooms = table.Column<int>(nullable: false),
                    NumberBathrooms = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    PropertyTypeId = table.Column<int>(nullable: false),
                    PropertyTypeId1 = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Properties", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Properties_PropertyTypes_PropertyTypeId1",
                        column: x => x.PropertyTypeId1,
                        principalTable: "PropertyTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Properties",
                columns: new[] { "Id", "Address", "DateAdded", "Deposit", "Description", "IsFurnished", "MonthlyPrice", "NumberBathrooms", "NumberBedrooms", "PropertyTypeId", "PropertyTypeId1" },
                values: new object[,]
                {
                    { 1L, "Dundee Loan, Forfar, Angus, DD8", new DateTime(2020, 8, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), 400m, "Beautiful 1 bedroom basement flat which has been fully renovated. Open plan lounge and kitchen with an integrated oven and hob. Good size bedroom with built in wardrobe space. Lovely shower room with ample storage and plumbing for a washing machine. Private gravel garden with clothes line. ", false, 400m, 1, 1, 4, null },
                    { 2L, "Pitreuchie Place, Forfar, Angus, DD8", new DateTime(2020, 8, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), 600m, "Immaculately presented 3 bedroom mid terrace house in a popular area of Forfar. Open plan lounge and dining area Kitchen with integrated oven and hob", false, 600m, 1, 3, 3, null },
                    { 3L, "Grampian Park, Forfar, Angus, DD8", new DateTime(2020, 8, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), 600m, "Immaculate and spacious 3 bedroom end terrace house. Early viewing recommended.", false, 600m, 1, 3, 2, null },
                    { 4L, "Merchiston Crescent, Edinburgh, Midlothian", new DateTime(2020, 8, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), 5000m, "Fantastic 5 bed HMO off Bruntsfield in the beautiful Merchiston area, close to excellent amenities, shops, bus links and opposite the Edinburgh Napier Merchiston campus.", true, 2500m, 2, 5, 4, null }
                });

            migrationBuilder.InsertData(
                table: "PropertyTypes",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1L, "Detached" },
                    { 2L, "Semi-detached" },
                    { 3L, "Terraced" },
                    { 4L, "Flat" },
                    { 5L, "Bungalow" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Properties_PropertyTypeId1",
                table: "Properties",
                column: "PropertyTypeId1");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Properties");

            migrationBuilder.DropTable(
                name: "PropertyTypes");
        }
    }
}
