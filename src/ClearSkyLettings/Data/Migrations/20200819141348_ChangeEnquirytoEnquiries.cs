﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ClearSkyLettings.Data.Migrations
{
    public partial class ChangeEnquirytoEnquiries : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Enquiry_Properties_PropertyId",
                table: "Enquiry");

            migrationBuilder.DropForeignKey(
                name: "FK_Enquiry_AspNetUsers_UserId",
                table: "Enquiry");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Enquiry",
                table: "Enquiry");

            migrationBuilder.RenameTable(
                name: "Enquiry",
                newName: "Enquiries");

            migrationBuilder.RenameIndex(
                name: "IX_Enquiry_UserId",
                table: "Enquiries",
                newName: "IX_Enquiries_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Enquiry_PropertyId",
                table: "Enquiries",
                newName: "IX_Enquiries_PropertyId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Enquiries",
                table: "Enquiries",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Enquiries_Properties_PropertyId",
                table: "Enquiries",
                column: "PropertyId",
                principalTable: "Properties",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Enquiries_AspNetUsers_UserId",
                table: "Enquiries",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Enquiries_Properties_PropertyId",
                table: "Enquiries");

            migrationBuilder.DropForeignKey(
                name: "FK_Enquiries_AspNetUsers_UserId",
                table: "Enquiries");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Enquiries",
                table: "Enquiries");

            migrationBuilder.RenameTable(
                name: "Enquiries",
                newName: "Enquiry");

            migrationBuilder.RenameIndex(
                name: "IX_Enquiries_UserId",
                table: "Enquiry",
                newName: "IX_Enquiry_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Enquiries_PropertyId",
                table: "Enquiry",
                newName: "IX_Enquiry_PropertyId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Enquiry",
                table: "Enquiry",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Enquiry_Properties_PropertyId",
                table: "Enquiry",
                column: "PropertyId",
                principalTable: "Properties",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Enquiry_AspNetUsers_UserId",
                table: "Enquiry",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
