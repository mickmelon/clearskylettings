﻿using ClearSkyLettings.Domain.Properties.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ClearSkyLettings.Controllers
{
    public class PropertiesController : Controller
    {
        private readonly IMediator _mediator;

        public PropertiesController(IMediator mediator)
        {
            _mediator = mediator;
        }

        // GET: Properties
        public async Task<IActionResult> Index()
        {
            var properties = await _mediator.Send(new GetAllPropertiesQuery());
            return View(properties);
        }

        // GET: Properties/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var property = await _mediator.Send(new GetPropertyByIdQuery(id.Value));
            if (property == null)
            {
                return NotFound();
            }

            return View(property);
        }

        // GET: Properties/Create
        public IActionResult Create()
        {
            return View();
        }
    }
}
