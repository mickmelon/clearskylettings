﻿using ClearSkyLettings.Domain.Enquiries.Commands;
using ClearSkyLettings.Domain.Enquiries.Queries;
using ClearSkyLettings.Models;
using ClearSkyLettings.Util.Extensions.Alerts;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ClearSkyLettings.Controllers
{
    public class EnquiriesController : Controller
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IMediator _mediator;

        public EnquiriesController(UserManager<IdentityUser> userManager, IMediator mediator)
        {
            _userManager = userManager;
            _mediator = mediator;
        }

        // GET: Enquiries
        [Authorize]
        public async Task<IActionResult> Index()
        {
            var userId = _userManager.GetUserId(User);
            var enquiries = await _mediator.Send(new GetAllUserEnquiriesQuery(userId));
            return View(enquiries);
        }

        // GET: Enquiries/Make/5
        public async Task<IActionResult> Make(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            // If logged in, make the enquiry immediately
            if (User.Identity.IsAuthenticated)
            {
                var userId = _userManager.GetUserId(User);
                var success = await _mediator.Send(new MakeEnquiryAsUserCommand(id.Value, userId));
                var actionResult = RedirectToAction(nameof(Index));

                if (success)
                {
                    return actionResult.WithSuccess("Success", "The enquiry was successfully made.");
                }

                return actionResult.WithDanger("Error", "You have already made an enquiry for that property.");
            }

            // If not logged in, show view for user to type in email
            return View(new MakeEnquiryViewModel
            {
                PropertyId = id.Value
            });
        }

        // POST: Enquiries/Make
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Make(MakeEnquiryViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var success = await _mediator.Send(new MakeEnquiryAsGuestCommand(viewModel.PropertyId, viewModel.Email));
                if (success)
                {
                    return RedirectToAction("Details", "Properties", new { id = viewModel.PropertyId })
                        .WithSuccess("Success", "The enquiry was successfully made.");
                }

                return RedirectToAction("Index", "Properties", new { })
                    .WithDanger("Error", "It was not possible to create the enquiry.");
            }

            return View(viewModel);
        }

        // GET: Enquiries/Details/5
        [Authorize]
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var userId = _userManager.GetUserId(User);
            var enquiry = await _mediator.Send(new GetUserEnquiryByIdQuery(userId, id.Value));
            if (enquiry == null)
            {
                return NotFound();
            }

            return View(enquiry);
        }
    }
}
