﻿using ClearSkyLettings.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace ClearSkyLettings.Util.Extensions
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PropertyType>().HasData(
                new PropertyType
                {
                    Id = 1,
                    Name = "Detached"
                },
                new PropertyType
                {
                    Id = 2,
                    Name = "Semi-detached"
                },
                new PropertyType
                {
                    Id = 3,
                    Name = "Terraced"
                },
                new PropertyType
                {
                    Id = 4,
                    Name = "Flat"
                },
                new PropertyType
                {
                    Id = 5,
                    Name = "Bungalow"
                }
            );

            modelBuilder.Entity<Property>().HasData(
                new Property
                {
                    Id = 1,
                    Address = "Dundee Loan, Forfar, Angus, DD8",
                    MonthlyPrice = 400,
                    Deposit = 400,
                    DateAdded = new DateTime(2020, 8, 14),
                    IsFurnished = false,
                    NumberBedrooms = 1,
                    NumberBathrooms = 1,
                    Description = "Beautiful 1 bedroom basement flat which has been fully renovated. Open plan lounge and kitchen with an integrated oven and hob. Good size bedroom with built in wardrobe space. Lovely shower room with ample storage and plumbing for a washing machine. Private gravel garden with clothes line. ",
                    PropertyTypeId = 4
                },
                new Property
                {
                    Id = 2,
                    Address = "Pitreuchie Place, Forfar, Angus, DD8",
                    MonthlyPrice = 600,
                    Deposit = 600,
                    DateAdded = new DateTime(2020, 8, 12),
                    IsFurnished = false,
                    NumberBedrooms = 3,
                    NumberBathrooms = 1,
                    Description = "Immaculately presented 3 bedroom mid terrace house in a popular area of Forfar. Open plan lounge and dining area Kitchen with integrated oven and hob",
                    PropertyTypeId = 3
                },
                new Property
                {
                    Id = 3,
                    Address = "Grampian Park, Forfar, Angus, DD8",
                    MonthlyPrice = 600,
                    Deposit = 600,
                    DateAdded = new DateTime(2020, 8, 11),
                    IsFurnished = false,
                    NumberBedrooms = 3,
                    NumberBathrooms = 1,
                    Description = "Immaculate and spacious 3 bedroom end terrace house. Early viewing recommended.",
                    PropertyTypeId = 2
                },
                new Property
                {
                    Id = 4,
                    Address = "Merchiston Crescent, Edinburgh, Midlothian",
                    MonthlyPrice = 2500,
                    Deposit = 5000,
                    DateAdded = new DateTime(2020, 8, 5),
                    IsFurnished = true,
                    NumberBedrooms = 5,
                    NumberBathrooms = 2,
                    Description = "Fantastic 5 bed HMO off Bruntsfield in the beautiful Merchiston area, close to excellent amenities, shops, bus links and opposite the Edinburgh Napier Merchiston campus.",
                    PropertyTypeId = 4
                }
            );
        }
    }
}
