﻿using System.ComponentModel.DataAnnotations;

namespace ClearSkyLettings.Models
{
    public class MakeEnquiryViewModel
    {
        [Required]
        public long PropertyId { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
    }
}
