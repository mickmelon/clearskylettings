﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ClearSkyLettings.Models
{
    public class Property
    {
        public long Id { get; set; }

        public string Address { get; set; }

        [DisplayName("Monthly Price")]
        public decimal MonthlyPrice { get; set; }

        public decimal Deposit { get; set; }

        [DisplayName("Date Added")]
        [DataType(DataType.Date)]
        public DateTime DateAdded { get; set; }

        [DisplayName("Furnished?")]
        public bool IsFurnished { get; set; }

        [DisplayName("No. Bedrooms")]
        public int NumberBedrooms { get; set; }

        [DisplayName("No. Bathrooms")]
        public int NumberBathrooms { get; set; }

        public string Description { get; set; }

        [DisplayName("Property Type")]
        public long PropertyTypeId { get; set; }
        public PropertyType PropertyType { get; set; }
    }
}
