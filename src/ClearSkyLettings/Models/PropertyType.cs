﻿using System.Collections.Generic;
using System.ComponentModel;

namespace ClearSkyLettings.Models
{
    public class PropertyType
    {
        public long Id { get; set; }

        [DisplayName("Property Type")]
        public string Name { get; set; }

        public IEnumerable<Property> Properties { get; set; }
    }
}
