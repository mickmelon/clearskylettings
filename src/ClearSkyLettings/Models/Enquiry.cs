﻿using Microsoft.AspNetCore.Identity;
using System;
using System.ComponentModel;

namespace ClearSkyLettings.Models
{
    public class Enquiry
    {
        public long Id { get; set; }

        [DisplayName("Date Enquired")]
        public DateTime DateEnquired { get; set; }

        public long PropertyId { get; set; }
        public Property Property { get; set; }

        public string UserId { get; set; }
        public IdentityUser User { get; set; }
    }
}
